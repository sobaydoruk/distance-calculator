import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { AzimuthDistanceCalculator } from './azimuth-distance';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {
   
  }

  calculate(){
    let p0 = {
      latitude: 38.4222744,
      longitude: 27.12990046,
      altitude: 4
    };

    let p1 = {
      latitude: 38.39145317,
      longitude: 27.06422925,
      altitude: 32
    };

    let calculator = new AzimuthDistanceCalculator();
    console.log(calculator.Calculate(p0, p1, true));
  }
}
